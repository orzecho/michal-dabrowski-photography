#!/bin/bash

input_dir="./public/images/landscape"
output_dir="./public/images/landscape"
max_dimension=10000

find $input_dir -type f -name "*.webp" -delete

# Create the output directory if it doesn't exist
mkdir -p "$output_dir"

# Iterate through all JPEG files in the input directory
for file in "$input_dir"/*.jpg; do
    # Extract the filename and extension
    filename=$(basename "$file")
    extension="${filename##*.}"

    # Generate the output filename with the .webp extension
    output_file="$output_dir/${filename%.*}.webp"

    # Get the original image dimensions
    width=$(identify -format "%w" "$file")
    height=$(identify -format "%h" "$file")

    # Check if the image needs to be scaled down
    if [[ $width -gt $max_dimension || $height -gt $max_dimension ]]; then
        # Calculate the scaling factor for the maximum dimension
        if [[ $width -gt $height ]]; then
            scale_factor=$(awk "BEGIN { printf \"%.2f\", $max_dimension / $width }")
        else
            scale_factor=$(awk "BEGIN { printf \"%.2f\", $max_dimension / $height }")
        fi

        # Calculate the new dimensions after scaling
        new_width=$(awk "BEGIN { printf \"%.0f\", $width * $scale_factor }")
        new_height=$(awk "BEGIN { printf \"%.0f\", $height * $scale_factor }")

        # Convert the JPEG to WebP with scaling using cwebp command
        cwebp -q 80 -resize "$new_width" "$new_height" "$file" -o "$output_file"

        echo "Converted $filename to ${filename%.*}.webp (scaled down)"
    else
        # Convert the JPEG to WebP without scaling using cwebp command
        cwebp -q 80 "$file" -o "$output_file"

        echo "Converted $filename to ${filename%.*}.webp"
    fi
done

echo "Conversion completed."
