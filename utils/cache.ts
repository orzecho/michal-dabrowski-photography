let cache = {};

export function get(key: string) {
    return cache[key];
}

export function put(key: string, value: any) {
    cache[key] = value;
}
