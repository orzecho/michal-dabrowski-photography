export function Footer() {
    return <footer className="p-6 text-center text-white/80 sm:p-12">
        Michał Dąbrowski Photography
    </footer>;
}
