import path from 'path';
import sharp from 'sharp';

export default async function getBase64ImageUrl(
  url: string, size: {width?: number, height?: number}
): Promise<{data: string, size: {height: number, width: number}}> {
  const sharpImage = sharp(path.join(process.cwd(), 'public', url));
  const buffer = await sharpImage.resize(size).toBuffer();
  const metadata = await sharp(buffer).metadata();
  return {
    data:`data:image/jpeg;base64,${buffer.toString('base64')}`,
    size: {width: metadata.width, height: metadata.height}
  }
}
