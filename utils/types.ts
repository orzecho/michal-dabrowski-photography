/* eslint-disable no-unused-vars */
export interface ImageProps {
  id: number
  height: number
  width: number
  public_id: string
  format: string
  blurDataUrl?: string
  thumbnailDataUrl?: string
  url?: string
  name?: string
}

export interface SharedModalProps {
  index: number
  images?: ImageProps[]
  currentPhoto?: ImageProps
  closeModal: () => void
  navigation: boolean
  direction?: number
  prefix?: string
}
