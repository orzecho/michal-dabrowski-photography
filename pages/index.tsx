import * as fs from 'fs';
import type { NextPage } from 'next'
import Head from 'next/head'
import Image from 'next/image'
import Link from 'next/link'
import { useRouter } from 'next/router'
import path from 'path';
import { useRef } from 'react'
import avatar from '../public/avatar.jpg';
import { get, put } from '../utils/cache';
import { Footer } from '../utils/footer';
import getBase64ImageUrl from '../utils/generateBlurPlaceholder'
import type { ImageProps } from '../utils/types'
import { useLastViewedPhoto } from '../utils/useLastViewedPhoto'


function aboutMe() {
  return <div
      className="after:content relative mb-5 flex h-[629px] flex-col items-center justify-end gap-4 overflow-hidden rounded-lg bg-white/10 px-6 pb-16 pt-64 text-center text-white shadow-highlight after:pointer-events-none after:absolute after:inset-0 after:rounded-lg after:shadow-highlight lg:pt-0">
    <Avatar />
    <h1 className="mt-8 mb-4 text-base font-bold uppercase tracking-widest">
      Michał Dąbrowski Photography
    </h1>
    <p className="max-w-[40ch] text-white/75 sm:max-w-[32ch]">
      Michał Dąbrowski is a portrait, landscape and documentary photographer based in Warsaw, Poland.
    </p>
  </div>;
}

export function getImages(images: ImageProps[], lastViewedPhoto, lastViewedPhotoRef, prefix: string = '/p') {
  return images.map(({id, public_id, blurDataUrl, url, thumbnailDataUrl, height, width, name}) => (
      <Link
          key={id}
          href={{
            pathname: `${prefix}/${id}`,
          }}
          ref={id === Number(lastViewedPhoto) ? lastViewedPhotoRef : null}
          shallow
          prefetch={true}
          className="after:content group relative mb-5 block w-full cursor-zoom-in after:pointer-events-none after:absolute after:inset-0 after:rounded-lg after:shadow-highlight"
      >
        <Image
            alt={public_id}
            className="transform rounded-lg brightness-90 transition will-change-auto group-hover:brightness-110"
            style={{transform: 'translate3d(0, 0, 0)'}}
            placeholder="blur"
            blurDataURL={blurDataUrl}
            src={thumbnailDataUrl}
            width={width}
            height={height}
            sizes="(max-width: 640px) 100vw,
                  (max-width: 1280px) 50vw,
                  (max-width: 1536px) 33vw,
                  25vw"
        />
      </Link>
  ));
}

const Avatar = () => {
  return (
      <div className={`rounded-full overflow-hidden m-8`}>
        <Image src={avatar} alt={'Michał Dąbrowski photo'} className="object-cover w-full h-full" />
      </div>
  )
}


export const Navbar = () => {
  return (
      <nav className="bg-slate-900 bg-opacity-50 px-4 py-2 mb-3 rounded-lg ">
        <div className="flex items-center justify-between">
          <Link href="/" className="text-white font-bold text-lg">Michał Dąbrowski Photography
          </Link>
          <div className="flex items-center">
            <Link href="/portrait" className="text-gray-300 mx-3 hover:text-white">
              Portrait
            </Link>
            <Link href="/projects" className="text-gray-300 mx-3 hover:text-white">
              Books & Projects
            </Link>
            <Link href="/landscape" className="text-gray-300 mx-3 hover:text-white">
              Landscape & nature (color)
            </Link>
          </div>
        </div>
      </nav>
  )
}

export const CommonHead = () => {
  return <Head>
    <title>Michał Dąbrowski Photography</title>
    <meta
        property="og:image"
        content="https://michal-dabrowski-photography.vercel.app/og-image.png"
    />
    <meta
        name="twitter:image"
        content="https://michal-dabrowski-photography.vercel.app/og-image.png"
    />
  </Head>;
}

const Home: NextPage = ({ images }: { images: ImageProps[] }) => {
  const router = useRouter()
  const [lastViewedPhoto, setLastViewedPhoto] = useLastViewedPhoto()

  const lastViewedPhotoRef = useRef<HTMLAnchorElement>(null)

  return (
    <>
      <CommonHead />
      <main className="mx-auto max-w-[1960px] p-4">
        <Navbar />

        <div className="columns-1 gap-4 sm:columns-2 xl:columns-3 2xl:columns-4">
          {aboutMe()}
          {getImages(images, lastViewedPhoto, lastViewedPhotoRef)}
        </div>
      </main>
      <Footer />
    </>
  )
}

export default Home

export async function getImageProps(cacheKey: string, imagesDirectory: string = path.join(process.cwd(), 'public', 'images', 'main'), directoryName: string = 'main') {
  const fromCache = get(cacheKey);
  if (!fromCache) {
    const imageFilenames = fs.readdirSync(imagesDirectory);
    const urls = imageFilenames.map(f => `/images/${directoryName}/${f}`);

    let reducedResults: ImageProps[] = []

    let i = 0
    for (let result of urls.filter(u => u.includes('.webp'))) {
      const thumbnailDataUrl = await getBase64ImageUrl(result, {width: 420});
      reducedResults.push({
        id: i,
        name: result.replaceAll('/images/', ''),
        height: thumbnailDataUrl.size.height,
        width: thumbnailDataUrl.size.width,
        public_id: result.replaceAll('.jpg', ''),
        format: 'jpg',
        url: result,
        blurDataUrl: (await getBase64ImageUrl(result, {width: 10})).data,
        thumbnailDataUrl: thumbnailDataUrl.data,
      })
      i++
    }
    put(cacheKey, reducedResults);
    return reducedResults;
  } else {
    return fromCache;
  }
}

export async function getStaticProps() {
  return {
    props: {
      images: await getImageProps('cachedImageProps'),
    },
  }
}
