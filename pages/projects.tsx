import type { NextPage } from 'next'
import atawizm from '../public/atawizm_grid_preview.webp';
import osmosis from '../public/osmosis_cover.webp';
import { Footer } from '../utils/footer';
import { ImageList } from '../components/imageList';
import { CommonHead, Navbar } from './index';


const Projects: NextPage = ({}: {}) => {
    const items = [
        {
            src: osmosis,
            title: 'Osmosis',
            text: 'Osmosis is the result of the Analogue Photography Workshops run by Federico Caponi at the Photography Department at the Academy of Photography in Warsaw. The book was published by The Foundation Council of the Visual Arts (Fundacja Kolegium Sztuk Wizualnych).'
        },
        {
            src: atawizm,
            title: 'Atawizm',
            text: 'The project tries to capture an aspect of living in the city that currently becomes more and more fleeting. For years the number of small shops has been dropping systematically while the only thriving sector of the retail market is large shops and chains. Even though we, as consumers, create this reality by choosing a convenience store near our home or by buying groceries online, there is a special quality in things old and seasoned that is hard to measure in terms of convenience. Even if this quality is not destined to survive, at least it deserves to be remembered.'
        },
        // {
        //     src: avatar,
        //     title: 'Portas',
        //     text: 'Project in progress.'
        // },
    ];
  return (
    <>
      <CommonHead />
      <main className="mx-auto max-w-[1960px] p-4">
        <Navbar />
        <ImageList items={items} />
      </main>
      <Footer />
    </>
  )
}

export default Projects

export async function getStaticProps() {
  return {
    props: {
    },
  }
}
