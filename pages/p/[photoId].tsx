import type { GetStaticProps, NextPage } from 'next'
import Head from 'next/head'
import { useRouter } from 'next/router'
import path from 'path';
import Carousel from '../../components/Carousel'
import type { ImageProps } from '../../utils/types'
import { getImageProps } from '../index';

export const CommonPhoto = ({currentPhoto, index, images, backTo, prefix}) => {
  return <>
    <Head>
      <title>Michał Dąbrowski Photography</title>
      <meta property="og:image" content={currentPhoto.url as string} />
      <meta name="twitter:image" content={currentPhoto.url as string} />
    </Head>
    <main className="mx-auto max-w-[1960px] p-4">
      <Carousel currentPhoto={currentPhoto} index={index} images={images} backTo={backTo} prefix={prefix} />
    </main>
  </>
}

const Photo: NextPage = ({ currentPhoto, images }: { currentPhoto: ImageProps, images: ImageProps[] }) => {
  const router = useRouter()
  const { photoId } = router.query
  return <CommonPhoto currentPhoto={currentPhoto} index={Number(photoId)} images={images} backTo={'/'} prefix={'/p'} />
}

export default Photo


export const getStaticProps: GetStaticProps = async (context) => {
  const imageProps = await getImageProps('mainPhotoCache', path.join(process.cwd(), 'public', 'images', 'main'), 'main');

  const currentPhoto = imageProps.find(
    (img) => img.id === Number(context.params.photoId)
  )

  return {
    props: {
      images: imageProps,
      currentPhoto: currentPhoto,
    },
  }
}

export async function getStaticPaths() {
  const imageProps = await getImageProps('mainPhotoCache', path.join(process.cwd(), 'public', 'images', 'main'), 'main');
  const fullPaths = imageProps.map(i => {
    return { params: { photoId: `${i.id}` } };
  });

  return {
    paths: fullPaths,
    fallback: false,
  }
}
