import type { GetStaticProps, NextPage } from 'next'
import { useRouter } from 'next/router'
import path from 'path';
import type { ImageProps } from '../../../utils/types'
import { getImageProps } from '../../index';
import { CommonPhoto } from '../../p/[photoId]';

const Photo: NextPage = ({ currentPhoto, images }: { currentPhoto: ImageProps, images: ImageProps[] }) => {
  const router = useRouter()
  const { photoId } = router.query
  return <CommonPhoto currentPhoto={currentPhoto} index={Number(photoId)} images={images} backTo={'/landscape'} prefix={'/landscape/p'}/>
}

export default Photo


export const getStaticProps: GetStaticProps = async (context) => {
  const imageProps = await getImageProps('landscapePhotoCache', path.join(process.cwd(), 'public', 'images', 'landscape'), 'landscape');

  const currentPhoto = imageProps.find(
    (img) => img.id === Number(context.params.photoId)
  )

  return {
    props: {
      images: imageProps,
      currentPhoto: currentPhoto,
    },
  }
}

export async function getStaticPaths() {
  const imageProps = await getImageProps('landscapePhotoCache', path.join(process.cwd(), 'public', 'images', 'landscape'), 'landscape');
  const fullPaths = imageProps.map(i => {
    return { params: { photoId: `${i.id}` } };
  });

  return {
    paths: fullPaths,
    fallback: false,
  }
}
