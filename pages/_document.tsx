import Document, { Head, Html, Main, NextScript } from 'next/document'

class MyDocument extends Document {
  render() {
    return (
      <Html lang="en">
        <Head>
          <link rel="icon" href="/favicon.ico" />
          <meta
            name="description"
            content="Michał Dąbrowski is a portrait, landscape and documentary photographer based in Warsaw, Poland."
          />
          <meta property="og:site_name" content="michal-dabrowski-photography.eu" />
          <meta
            property="og:description"
            content="Michał Dąbrowski is a portrait, landscape and documentary photographer based in Warsaw, Poland."
          />
          <meta property="og:title" content="Michał Dąbrowski Photography" />
          <meta name="twitter:card" content="summary_large_image" />
          <meta name="twitter:title" content="Michał Dąbrowski Photography" />
          <meta
            name="twitter:description"
            content="Michał Dąbrowski is a portrait, landscape and documentary photographer based in Warsaw, Poland."
          />
        </Head>
        <body className="bg-black antialiased">
          <Main />
          <NextScript />
        </body>
      </Html>
    )
  }
}

export default MyDocument
