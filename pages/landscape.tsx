import type { NextPage } from 'next'
import path from 'path';
import { useRef } from 'react'
import { Footer } from '../utils/footer';
import type { ImageProps } from '../utils/types'
import { useLastViewedPhoto } from '../utils/useLastViewedPhoto'
import { CommonHead, getImageProps, getImages, Navbar } from './index';


const Landscape: NextPage = ({ images }: { images: ImageProps[] }) => {
  const [lastViewedPhoto, setLastViewedPhoto] = useLastViewedPhoto()

  const lastViewedPhotoRef = useRef<HTMLAnchorElement>(null)

  return (
    <>
      <CommonHead />
      <main className="mx-auto max-w-[1960px] p-4">
        <Navbar />

        <div className="columns-1 gap-4 sm:columns-2 xl:columns-3 2xl:columns-4">
          {getImages(images, lastViewedPhoto, lastViewedPhotoRef, '/landscape/p')}
        </div>
      </main>
      <Footer />
    </>
  )
}

export default Landscape

export async function getStaticProps() {
  return {
    props: {
      images: await getImageProps('landscapeCache', path.join(process.cwd(), 'public', 'images', 'landscape'), 'landscape'),
    },
  }
}
