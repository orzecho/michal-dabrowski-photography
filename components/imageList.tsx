import Image from 'next/image';

export const ImageList = ({ items }) => {
    return (items.map((item, index) => (
                <div className="grid grid-cols-12 gap-4">
                    <div key={index} className="lg:col-span-2 col-span-2" />
                    <div key={index} className="lg:col-span-3 col-span-8 mx-4 my-2">
                        <Image src={item.src} alt={item.alt} className="rounded-full" />
                    </div>
                    <div className='lg:col-span-5 col-span-10 py-10'>
                        <h2 className="text-white text-xl">{item.title}</h2>
                        <p className="text-gray-300 text-justify">{item.text}</p>
                    </div>
                </div>
                )));
}

