import { ChevronLeftIcon, ChevronRightIcon, XMarkIcon } from '@heroicons/react/24/outline'
import { AnimatePresence, motion, MotionConfig } from 'framer-motion'
import Image from 'next/image'
import Link from 'next/link';
import { useState } from 'react'
import { useSwipeable } from 'react-swipeable'
import { variants } from '../utils/animationVariants'
import { range } from '../utils/range'
import type { ImageProps, SharedModalProps } from '../utils/types'

export default function SharedModal({
                                      index,
                                      images,
                                      closeModal,
                                      navigation,
                                      currentPhoto,
                                      direction,
                                      prefix
                                    }: SharedModalProps) {
  const [loaded, setLoaded] = useState(false)
  const [currentPhotoIndex, setCurrentPhotoIndex] = useState(index);

  let filteredImages = images?.filter((img: ImageProps) =>
    range(currentPhotoIndex - 15, currentPhotoIndex + 15).includes(img.id)
  )

  const handlers = useSwipeable({
    onSwipedLeft: () => {
      if (currentPhotoIndex < images?.length - 1) {
        setCurrentPhotoIndex(currentPhotoIndex + 1)
      }
    },
    onSwipedRight: () => {
      if (currentPhotoIndex > 0) {
        setCurrentPhotoIndex(currentPhotoIndex - 1);
      }
    },
    trackMouse: true,
  })

  let currentImage = images ? images[currentPhotoIndex] : currentPhoto

  return (
    <MotionConfig
      transition={{
        x: { type: 'spring', stiffness: 300, damping: 30 },
        opacity: { duration: 0.2 },
      }}
    >
      <div
        className="relative z-50 flex aspect-[3/2] w-full max-w-7xl items-center wide:h-full xl:taller-than-854:h-auto"
        {...handlers}
      >
        {/* Main image */}
        <div className="w-full overflow-hidden">
          <div className="relative flex aspect-[3/2] justify-center">
            <AnimatePresence initial={false} custom={direction}>
              <motion.div
                key={currentPhotoIndex}
                custom={direction}
                variants={variants}
                initial="enter"
                animate="center"
                exit="exit"
              >
                <Image
                  src={currentImage.url}
                  fill
                  priority
                  objectFit={"contain"}
                  alt="Next.js Conf image"
                  onLoadingComplete={() => setLoaded(true)}
                />
              </motion.div>
            </AnimatePresence>
          </div>
        </div>

        {/* Buttons + bottom nav bar */}
        <div className="absolute inset-0 mx-auto flex max-w-7xl items-center justify-center">
          {/* Buttons */}
          {loaded && (
            <div className="relative aspect-[3/2] max-h-full w-full">
              {navigation && (
                <>
                  {currentPhotoIndex > 0 && (
                      <Link
                          key={currentPhotoIndex - 1}
                          href={`${prefix}/${currentPhotoIndex - 1}`}
                          onClick={() => setCurrentPhotoIndex(currentPhotoIndex - 1)}
                          shallow
                          className="absolute left-3 top-[calc(50%-16px)] rounded-full bg-black/50 p-3 text-white/75 backdrop-blur-lg transition hover:bg-black/75 hover:text-white focus:outline-none"
                      > <ChevronLeftIcon className="h-6 w-6" />
                       </Link>
                  )}
                  {currentPhotoIndex + 1 < images.length && (
                      <Link
                          key={currentPhotoIndex + 1}
                          href={`${prefix}/${currentPhotoIndex + 1}`}
                          onClick={() => setCurrentPhotoIndex(currentPhotoIndex + 1)}
                          shallow
                          className="absolute right-3 top-[calc(50%-16px)] rounded-full bg-black/50 p-3 text-white/75 backdrop-blur-lg transition hover:bg-black/75 hover:text-white focus:outline-none"
                      > <ChevronRightIcon className="h-6 w-6" />
                      </Link>
                  )}
                </>
              )}
              <div className="absolute top-0 left-0 flex items-center gap-2 p-3 text-white">
                <button
                  onClick={() => closeModal()}
                  className="rounded-full bg-black/50 p-2 text-white/75 backdrop-blur-lg transition hover:bg-black/75 hover:text-white"
                >
                    <XMarkIcon className="h-5 w-5" />
                </button>
              </div>
            </div>
          )}
          {/* Bottom Nav bar */}
          {navigation && (
            <div className="fixed inset-x-0 bottom-0 z-40 overflow-hidden bg-gradient-to-b from-black/0 to-black/60">
              <motion.div
                initial={false}
                className="mx-auto mt-6 mb-6 flex aspect-[3/2] h-14"
              >
                <AnimatePresence initial={false}>
                  {filteredImages.map(({ id, thumbnailDataUrl }) => (
                    <motion.button
                      initial={{
                        width: '0%',
                        x: `${Math.max((currentPhotoIndex - 1) * -100, 15 * -100)}%`,
                      }}
                      animate={{
                        scale: id === currentPhotoIndex ? 1.25 : 1,
                        width: '100%',
                        x: `${Math.max(currentPhotoIndex * -100, 15 * -100)}%`,
                      }}
                      exit={{ width: '0%' }}
                      onClick={() => setCurrentPhotoIndex(id)}
                      key={id}
                      className={`${
                        id === currentPhotoIndex
                          ? 'z-20 rounded-md shadow shadow-black/50'
                          : 'z-10'
                      } ${id === 0 ? 'rounded-l-md' : ''} ${
                        id === images.length - 1 ? 'rounded-r-md' : ''
                      } relative inline-block w-full shrink-0 transform-gpu overflow-hidden focus:outline-none`}
                    >
                      <Image
                        alt="small photos on the bottom"
                        width={180}
                        height={120}
                        className={`${
                          id === currentPhotoIndex
                            ? 'brightness-110 hover:brightness-110'
                            : 'brightness-50 contrast-125 hover:brightness-75'
                        } h-full transform object-cover transition`}
                        src={thumbnailDataUrl}
                      />
                    </motion.button>
                  ))}
                </AnimatePresence>
              </motion.div>
            </div>
          )}
        </div>
      </div>
    </MotionConfig>
  )
}
